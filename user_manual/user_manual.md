# User Manual for the iFado platform

## In this user manual, it is expected that you are in the iFADO / MyCOAST machine and in /srv/http/iFADO/index_iFADO.html

### Changing the default background map

To change the default map in the background, open *index_iFADO.html* in a text/code editor like VSCode and in line 947 you can find every map related option in the website.

To select a default map, you need to have a valid, uncommented map object already setup, much like

```javascript
new OpenLayers.Layer.WMS("GEBCO bathymetry Latest",
    "https://www.gebco.net/data_and_products/gebco_web_services/web_map_service/mapserv", {
    "layers": "GEBCO_Latest",
    "version": "1.3.0",
    "format": "image/png",
    }, {
    "isBaseLayer": true,
    "sphericalMercator": true,
    "numZoomLevels": 22,
    "wrapDateLine": false
    }
),
```

and place it at the first position of the baseLayers array (right below *baseLayer: [*, don't forget the comma (,) at he end if you want more than one map and no errors)

### Adding a new domain / depth

In order to add a new domain or depth, go to line 1085. You will need to have a valid product so it will get displayed. An example is the nitrate in the Portugal (Surface) domain

```javascript
{
    label: external('currentColor') + 'Nitrate (MOHID)',
    id: '3857_ifado__0m_nitrate_portugal',
    type: 'NCWMS',
    selected: false,
    mustBeCurrent: true,
    opacity: 1,
    stackLevel: 0,
    description: "mole concentration of nitrate in sea water",

    ncwmsURL: 'http://thredds.maretec.org/thredds/wms/IST_MOHID_BIO_DATA/PORTUGAL_0.06DEG_50L_3H/FORECAST',
    ncwmsQueryAdapter: mohidTHREDDSSurface,
    ncwmsParams: {
        TRANSPARENT: true,
        COLORSCALERANGE: '0,0.2',
        NUMCOLORBANDS: 256,
        ABOVEMAXCOLOR: '0x000000',
        BELOWMINCOLOR: '0x000000',
        LOGSCALE: 'false',
        // NUMCONTOURS: 10,
        LAYERS: 'nitrate',
        STYLES: 'boxfill/ferret',
        CRS: 'EPSG:3857',
        VERSION: '1.3.0',
    },
    infoURL: 'resources/descriptions/product_3857_ifado__0m_nitrate.md'
},
```

Useful information:

#### FORMAT

The supported formats are:
    image/png
    image/png;mode=32bit
    image/gif
    image/jpeg
    application/vnd.google-earth.kmz

#### TIME

For gridded data with a discrete time axis this takes a single value. For in-situ data which is spread over a range, it is more useful to provide a time range in the form starttime/endtime. This should ideally be used in conjunction with the TARGETTIME parameter (see below)

#### ELEVATION

For gridded data with a discrete vertical axis this takes a single value. For in-situ data which is spread over a range, it is more useful to provide an elevation range in the form startelevation/endelevation. This should ideally be used in conjunction with the TARGETELEVATION parameter (see below)

#### STYLES

Available styles can be found from the Capabilities document. Generally styles are of the form stylename/palettename, and the style name default is always available

In additional to the standard GetMap parameters, ncWMS accepts the following optional additional parameters. If not specified, the server-configured defaults are used:

#### CRS

The projection of the layer. Default is CRS:84, supported formats are:
    CRS:84
    EPSG:4326
    EPSG:3857
    EPSG:900913
    EPSG:32661
    EPSG:32761
    EPSG:5401
    EPSG:5402

#### VERSION

ncWMS version.

#### COLORSCALERANGE

Of the form min,max this is the scale range used for plotting the data.
**Note: With this parameter, for now, the corrections have to be done manually until data shows up on the website**

#### NUMCOLORBANDS

The number of discrete colours to plot the data. Must be between 2 and 250

#### ABOVEMAXCOLOR

The colour to plot values which are above the maximum end of the scale range. Colours are as defined above, with the addition of "extend", which will use the maximum value of the palette.

#### BELOWMINCOLOR

The colour to plot values which are below the minimum end of the scale range. Colours are as defined above, with the addition of "extend", which will use the minimum value of the palette.

#### LOGSCALE

"true" or "false" - whether to plot data with a logarithmic scale

#### TARGETTIME

For in-situ data, all points which fall within the time range (specified in the TIME parameter) will be plotted. In the case that an in-situ point has multiple time readings within that range, the colour used to plot them will depend on the time value which is closest to this given value

#### TARGETELEVATION

For in-situ data, all points which fall within the elevation range (specified in the ELEVATION parameter) will be plotted. In the case that an in-situ point has multiple elevation readings within that range, the colour used to plot them will depend on the elevation value which is closest to this given value

#### OPACITY

The percentage opacity of the final output image

#### ANIMATION

"true" or "false" - whether to generate an animation. This also needs the TIME to be of the formstarttime/endtime, and currently is only implemented for features with a discrete time axis.

### Changing the information in each layer

To change the information in each layer refer to the [documentation](https://docs.geoserver.org/stable/en/user/services/wms/reference.html#wms-getmap). The most importnat parameters are explained in the previous point

### Changing the color scale

Refer to the STYLES parameter in **Adding a new domain / depth**:
To change the color scale and style shown in each layer, choose any domain currently in the website and select the STYLES paramater in **ncwmsParams** and choose your preferred style from [styles](http://thredds.maretec.org/thredds/wms/MOHID_WATER/PORTUGAL_0.06DEG_50L_3H/FORECAST/2018010200.nc?service=WMS&version=1.3.0&request=GetCapabilities)
